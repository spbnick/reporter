from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch

from reporter.data import CheckoutData

from .mocks import MOCK_CHECKOUT_ID
from .mocks import get_mocked_build
from .mocks import get_mocked_checkout
from .mocks import get_mocked_issue_occurrence
from .mocks import get_mocked_kcidbfile
from .mocks import get_mocked_test


class TestReporterData(TestCase):
    """Tests for reporter/data.py ."""

    @patch('reporter.data.urlopen')
    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data(self, checkouts_get_mock, urlopen_mock):
        """Test the CheckoutData class and it's properties."""

        tests = [get_mocked_test('PASS', False)]
        builds = [get_mocked_build(True, tests)]
        issues = [get_mocked_issue_occurrence()]
        checkout = get_mocked_checkout(True, builds, issues)

        kcidbfile = get_mocked_kcidbfile(builds=builds, checkouts=[checkout], issues=issues,
                                         tests=tests)

        checkout_id = MOCK_CHECKOUT_ID
        checkouts_get_mock.return_value.all.get.return_value = kcidbfile
        checkout_data = CheckoutData(checkout_id)

        checkouts_get_mock.assert_called_with(id=checkout_id)

        self.assertEqual(checkout_data.checkout, checkout)

        merge_log = MagicMock()
        merge_log.read.side_effect = [b'bar']
        urlopen_mock.return_value.__enter__.return_value = merge_log

        self.assertEqual(checkout_data.mergelog, 'bar')
        self.assertEqual(checkout_data.issues_occurrences, issues)

        self.assertEqual(checkout_data.build_data.builds, builds)
        self.assertEqual(checkout_data.test_data.tests, tests)

        self.assertTrue(checkout_data.result)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_failed_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        checkout = get_mocked_checkout(True, builds)

        kci_file = get_mocked_kcidbfile(builds=builds, checkouts=[checkout], tests=tests, issues=[])

        checkouts_get_mock.return_value.all.get.return_value = kci_file
        checkout_data = CheckoutData('1234')

        self.assertFalse(checkout_data.result)
        self.assertFalse(checkout_data.summary)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_known_issue_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        checkout = get_mocked_checkout(True, builds)

        kcidbfile = get_mocked_kcidbfile(builds=builds, checkouts=[checkout], tests=tests)

        checkouts_get_mock.return_value.all.get.return_value = kcidbfile
        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)
        self.assertTrue(checkout_data.summary)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_summary(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData
        tests = [get_mocked_test('MISS')]
        builds = [get_mocked_build(True, tests)]
        checkout = get_mocked_checkout(True, builds)

        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)
        self.assertIsNone(checkout_data.test_data.summary)
        self.assertIsNone(checkout_data.summary)

    def test_build_data(self):
        """Test the BuildData class and it's properties."""
        from reporter.data import BuildData

        tests = []
        passed_build = get_mocked_build(True, tests, id='passed_id')
        failed_build = get_mocked_build(False, tests, id='failed_id')
        issue = get_mocked_issue_occurrence()
        known_failure_build = get_mocked_build(False, tests)

        all_data = MagicMock(builds=[passed_build, failed_build, known_failure_build],
                             issueoccurrences=[issue])

        build_data = BuildData(all_data)

        self.assertEqual(build_data.passed_builds, [passed_build])
        self.assertEqual(build_data.failed_builds,
                         [failed_build, known_failure_build])
        self.assertEqual(build_data.known_issues_builds,
                         [known_failure_build])
        self.assertEqual(build_data.unknown_issues_builds, [failed_build])

    def test_build_data_get_kernel_variant(self):
        """Test the BuildData get_kernel_variant static method."""
        from reporter.data import BuildData
        build_default = get_mocked_build(misc={'debug': False, 'package_name': 'kernel'})
        build_debug = get_mocked_build(misc={'debug': True, 'package_name': 'kernel'})
        build_64k = get_mocked_build(misc={'debug': False, 'package_name': 'kernel-64k'})
        build_64k_debug = get_mocked_build(misc={'debug': True, 'package_name': 'kernel-64k'})
        build_64k_debug_package = get_mocked_build(misc={'debug': False,
                                                         'package_name': 'kernel-64k-debug'})
        build_64k_debug_both = get_mocked_build(misc={'debug': True,
                                                      'package_name': 'kernel-64k-debug'})

        self.assertEqual(BuildData.get_kernel_variant(build_default), '')
        self.assertEqual(BuildData.get_kernel_variant(build_debug), 'debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k), '64k')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug_package), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug_both), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k, 'kernel-64k'), '')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug, 'kernel-64k'), 'debug')

    def test_test_data(self):
        """Test the TestData class and it's properties."""
        from reporter.data import TestData
        pass_test = get_mocked_test('PASS', False)
        pass_waived_test = get_mocked_test('PASS', True)
        fail_test = get_mocked_test('FAIL', False)
        fail_waived_test = get_mocked_test('FAIL', True)
        error_test = get_mocked_test('ERROR', False)
        miss_test = get_mocked_test('MISS', False)
        none_test = get_mocked_test(None, False)
        known_issue_occurrence = get_mocked_issue_occurrence(
            issue_attributes={'description': 'failed', 'resolved': False}
        )
        fail_known_test1 = get_mocked_test('FAIL', False, [known_issue_occurrence])
        fail_known_test2 = get_mocked_test('FAIL', False, [known_issue_occurrence])
        regression = get_mocked_test('FAIL', False, [get_mocked_issue_occurrence(
            is_regression=True,
            issue_attributes={'description': 'failed', 'resolved': True}
        )])
        tests = [pass_test, fail_test, pass_waived_test, fail_waived_test,
                 error_test, miss_test, none_test, fail_known_test1, fail_known_test2, regression]

        kcidbfile = MagicMock(tests=tests)
        test_data = TestData(kcidbfile)

        self.assertEqual(test_data.tests, tests)

        self.assertEqual(test_data.failed_tests,
                         [fail_test, fail_known_test1, fail_known_test2, regression])
        self.assertEqual(test_data.passed_tests,
                         [pass_test, pass_waived_test])
        self.assertEqual(test_data.missed_tests, [miss_test, none_test])
        self.assertEqual(test_data.nonmissed_tests,
                         [test for test in tests
                          if test not in test_data.missed_tests])
        self.assertEqual(test_data.errored_tests, [error_test])
        self.assertEqual(test_data.waived_tests,
                         [pass_waived_test, fail_waived_test])
        self.assertEqual(test_data.known_issues_tests, [fail_known_test1, fail_known_test2])
        self.assertEqual(test_data.unknown_issues_tests,
                         [fail_test])  # I'm not expecting the waived test here
        self.assertEqual(test_data.regressions, [regression])
        known_issue = fail_known_test1.issues_occurrences[0].issue
        self.assertEqual(test_data.known_issues,
                         {known_issue['ticket_url']: {'issue': known_issue,
                                                      'tests': [fail_known_test1, fail_known_test2]}
                          }
                         )

    def test_test_data_all_missed_check(self):
        """Test the TestData's all_missed_check method."""
        from reporter.data import TestData
        pass_test = get_mocked_test('PASS', False)
        miss_test1 = get_mocked_test('MISS', False)
        miss_test2 = get_mocked_test('MISS', False)

        kcidbfile_missed = get_mocked_kcidbfile(tests=[miss_test1])
        kcidbfile_nonmissed = get_mocked_kcidbfile(tests=[miss_test2, pass_test])

        test_data_missed = TestData(kcidbfile_missed)
        test_data_nonmissed = TestData(kcidbfile_nonmissed)

        self.assertTrue(test_data_missed.all_missed_check)
        self.assertFalse(test_data_nonmissed.all_missed_check)

    def test_test_data_all_missed_nonwaived_check(self):
        """Test the TestData's all_missed_nonwaived_check method."""
        from reporter.data import TestData
        pass_test_waived = get_mocked_test('PASS', True)
        miss_test_nonwaived = get_mocked_test('MISS', False)
        miss_test_waived1 = get_mocked_test('MISS', True)
        miss_test_waived2 = get_mocked_test('MISS', True)

        kcidbfile_waived_pass = get_mocked_kcidbfile(tests=[miss_test_waived1, pass_test_waived])
        kcidbfile_missed_nonwaived = get_mocked_kcidbfile(tests=[miss_test_nonwaived])
        kcidbfile_missed_waived = get_mocked_kcidbfile(tests=[miss_test_waived2])

        test_data_waived_pass = TestData(kcidbfile_waived_pass)
        test_data_missed_nonwaived = TestData(kcidbfile_missed_nonwaived)
        test_data_missed_waived = TestData(kcidbfile_missed_waived)

        self.assertFalse(test_data_waived_pass.all_missed_check)
        self.assertTrue(test_data_waived_pass.all_missed_nonwaived_check)
        self.assertTrue(test_data_missed_nonwaived.all_missed_check)
        self.assertTrue(test_data_missed_nonwaived.all_missed_nonwaived_check)
        self.assertTrue(test_data_missed_waived.all_missed_check)
        self.assertTrue(test_data_missed_waived.all_missed_nonwaived_check)
